# Bananes Export

BananesExport est une entreprise d'exportation de bananes imaginaire créer par AIOS SH pour évaluer diffrentes compétences, pratiques et maturités pour des développeurs Front ou Back ainsi que d'administration système/déploiement continu.

## Développement

Deux exercices sont disponibles pour la partie développement :
- Test Dev Back : [Back/README.md](Back/README.md)
- Test Dev Front : [Front/README.md](Front/README.md)

Le test dure entre 2 et 4h. Il est à réaliser d'où vous le souhaitez.

Le code est à partager de préférence via un repository git (gitlab, github ou autre).

## Administration système et déploiement continu

L'exercice de test des déploiements et d'administration systèmes se trouve dans le répertoire [Deploy](Deploy/README.md). Il devrait également demander entre 2 et 4 heures et peut être réalisé d'où vous le souhaitez.

## Debrief

Nous réaliserons ensuite ensemble une revue de code dans nos locaux du [20 rue des Petits Champs, 75002 PARIS](https://citymapper.com/directions?endaddress=Paris%2C+France&endcoord=48.866853%2C2.336898&endname=20+Rue+des+Petits+Champs&startcoord=48.861815%2C2.346831&startname=Ch%C3%A2telet+-+Les+Halles).

administration@aios.sh
